require 'savon'

module Trinity
    class Client

        def initialize url
            # create a client for the service
            @client = Savon.client(wsdl: url)
        end

        def operations
            @client.operations
        end

        def call operation, params
            @client.call(operation, params).body
        end
    end
end
# response.body
# => { find_user_response: { id: 42, name: 'Hoff' } }