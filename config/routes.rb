Rails.application.routes.draw do

    devise_for :users
    namespace :admin do
        root to: 'dashboard#index', as: 'dashboard'
        get 'dashboard/accounts', to: 'dashboard#accounts'
        get 'dashboard/online', to: 'dashboard#online'
        resources :users
    end
    match "*path", to: "pages#show", via: :all
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    root to: "pages#index"


end
