require 'digest/sha1'

module Devise
  module Encryptable
    module Encryptors
      class Sha1 < Base
        def self.digest(password, _stretches, salt, _pepper)
          str = "#{salt.upcase}:#{password.upcase}"
          Digest::SHA1.hexdigest(str).upcase
        end
      end
    end
  end
end