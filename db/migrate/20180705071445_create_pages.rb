class CreatePages < ActiveRecord::Migration[5.1]
  def change
    create_table :pages do |t|
      t.string :slug
      t.text :title
      t.text :description
      t.text :body
      t.integer :sort_order, default: 0
      t.boolean :show_in_nav, default: false
      t.text :nav_title

      t.timestamps
    end
  end
end
