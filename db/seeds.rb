# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

home_page = Page.find_or_initialize_by(slug: 'home') do |page|
    page.title = 'The Fallen Realms'
    page.description = "The Fallen Realms is a new Wrath of the Lich King PvE Server"
end
home_page.body = %s{
<div class="card">
    <div class="card-header">Welcome</div>
    <div class="card-body">
        <p>The Fallen Realms is a new private server that launched 25 June, 2018 with Wrath of the Lich King supprt.</p> <p>Our initial server, Savage Plains, is a PvE server with 7x experience rates, and increased drop rates to support that.</p>
        </div>
    </div>
</div>
}

home_page.save

guide = Page.find_or_initialize_by(slug: 'connecting') do |page|
    page.title = "Connection Guide | The Fallen Realms"
    page.nav_title = "Connection Guide"
    page.show_in_nav = true
    page.sort_order = 0
    page.body = %s{
    <div class="row justify-content-center"><div class="col-md-8"><div class="card"><div class="card-header">How To Connect</div> <div class="card-body"><ol><li>Install World of Warcraft (Patch 3.3.5a). (<a href="https://thepiratebay.pet/torrent/6687518/WOW_Wotlk_3.3.5a">Torrent Link</a>)</li> <li>If you already have the client, Open up the "World of Warcraft" directory. The default directory is "C:\Program Files\World of Warcraft".</li> <li>Open up the file called "realmlist.wtf" with a text editor such as Notepad. "realmlist.wtf" will be located in the directory `Data/enGB` underneath your primary "World of Warcraft" directory. To do this, you must right click on the file and choose properties, then select notepad as the default software for files with the ".wtf" ending. You may also just start the text editor and drag the file into the edit window.</li> <li><p>Erase all text and change it to:</p> <pre><code>
set realmlist logon.thefallenrealms.com
set realmname "Fallen Realms"
</code></pre> <p>Save the realmlist.wtf file and you may now start playing!</p> <p></p><strong>Important</strong>: Please delete your CACHE files for a proper gameplay.<p></p></li></ol></div></div></div></div>
}
end



guide.save