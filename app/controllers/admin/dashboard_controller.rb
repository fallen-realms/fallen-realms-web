module Admin
    class DashboardController < AdminController
        def index
            @online_characters = Character.online.paginate(page: params[:character_page] || 1, per_page: 25)
        end

        def accounts
            render json: {
                accounts: User.count,
                change: User.where("joindate > ?", 24.hours.ago).count
            }
        end

        def online
            render json: {
                online: Character.online.count,
            }
        end
    end
end
