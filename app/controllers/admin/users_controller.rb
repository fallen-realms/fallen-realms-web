module Admin
    class UsersController < AdminController
        def index
            @users = User.order(id: :asc).paginate(:page => params[:page] || 1)#.select('"users".*,')
            @character_counts = Character.group(:account).count
            @online = Character.select(:account).group(:account).where(online: 1).pluck(:account)
        end

        def show
            @user = User.find(params[:id])
        end
        # def collection

        # end

        # def model
        #     User.find(params[])
        # end
    end
end
