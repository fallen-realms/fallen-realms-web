class AdminController < ApplicationController
    before_action :authenticate_gm!
    layout 'admin/layouts/application'
    protected
    def authenticate_gm!
        redirect_to(new_user_session_path) unless current_user && current_user.gm?
    end
end
