class PagesController < ApplicationController
    def index
        @realms = Realm.all
        @page = Page.find_by(slug: 'home')
        raise ActionController::RoutingError.new('Not Found') if @page.nil?
    end

      def show
        path = params[:path] || params[:name]
        puts "About to load #{path} page"
        @page = Page.find_by(slug: path)
        raise ActionController::RoutingError.new('Not Found') if @page.nil?
      end
end
