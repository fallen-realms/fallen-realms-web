class Realm < AuthRecord
    self.table_name = "realmlist"
    def online
        @online ||= Character.where(online: 1).count
    end

    def to_hash
        {
            'name' => name,
            'online' => online
        }
    end
end
