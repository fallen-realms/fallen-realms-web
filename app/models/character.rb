class Character < CharactersRecord
    self.table_name = "characters"

    scope :online, -> { where(online: 1) }
    # alias_attribute :character_class, :class
    # self.ignored_columns = %w(class)
    # self.renamed_columns

    def class_name
        # 1 1   Warrior
        #  2   2   Paladin
        #  3   4   Hunter
        #  4   8   Rogue
        #  5   16  Priest
        #  6   32  Death Knight (3.x)
        #  7   64  Shaman
        #  8   128 Mage
        #  9   256 Warlock
        #  10  512 Monk (5.x)
        #  11  1024    Druid
        case attributes['class']
        when 1
            "Warrior"
        when 2
            "Paladin"
        when 3
            "Hunter"
        when 4
            "Rogue"
        when 5
            "Priest"
        when 6
            "Death Knight"
        when 7
            "Shaman"
        when 8
            "Mage"
        when 9
            "Warlock"
        when 11
            "Druid"
        end
    end

    def race_name
        case race
        when 1
            "Human"
        when 2
            "Orc"
        when 3
            "Dwarf"
        when 4
            "Night Elf"
        when 5
            "Undead"
        when 6
            "Tauren"
        when 7
            "Gnome"
        when 8
            "Troll"
        when 10
            "Blood Elf"
        when 11
            "Draenei"
        end
    end
end
