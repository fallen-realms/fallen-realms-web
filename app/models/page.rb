class Page < ApplicationRecord
    def content
        body
        @template ||= Liquid::Template.parse(body) # Parses and compiles the template
        # @template.registers["realms"] =
        @template.render().html_safe
    end
end

module PluralFilter
  def to_plural(count, word)
    puts "about to pluralize #{word}"
    word.pluralize(count)
  end
end