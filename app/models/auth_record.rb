class AuthRecord < ActiveRecord::Base
  self.abstract_class = true
  establish_connection :"auth_#{Rails.env}"
end
