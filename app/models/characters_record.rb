class CharactersRecord < ActiveRecord::Base
  self.abstract_class = true
  establish_connection :"characters_#{Rails.env}"
end
