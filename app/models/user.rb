# require Rails.root.join('lib', 'devise', 'encryptors', 'sha1')

class User < AuthRecord
    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable
    devise :database_authenticatable, :registerable, :encryptable,
        :recoverable, :rememberable, :validatable, :encryptor => :sha1

    self.table_name = "account"
    has_many :account_accesses, foreign_key: :id
    has_many :characters, foreign_key: :account

    alias_attribute 'encrypted_password', 'sha_pass_hash'
    validates :username, presence: :true, uniqueness: { case_sensitive: true }, length: { maximum: 16}

    def self.find_for_database_authentication(warden_conditions)
      conditions = warden_conditions.dup
      username = conditions.delete(:username)
      where(conditions.to_h).where(username: username.upcase).first
    end

    def password_salt
      username
    end
    def password_salt= _a
    end

    def admin?
      gm_level == 3
    end

    def gm?
      gm_level > 0
    end

    def gm_level
      @gm_level ||= account_accesses.maximum(:gmlevel)
    end

    def online?
      characters.where(online: 1).any?
    end
end
