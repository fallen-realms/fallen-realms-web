var helpers = {
    accounts: function () {
        $.getJSON("/admin/dashboard/accounts", function(data) {
            var accounts = $('#accounts');
            accounts.text(data.accounts);
            if(data.change > 1){
                accounts.addClass('bgc-green-50 c-green-500');
                accounts.removeClass('bgc-red-50 c-red-500');
            } else {
                accounts.addClass('bgc-red-50 c-red-500');
                accounts.removeClass('bgc-green-50 c-green-500');
            }
        });
    },

    online: function() {
        // onlinecount
        $.getJSON("/admin/dashboard/online", function(data) {
            var online = $('#onlineCount');
            online.text(data.online);
        });
    }
}